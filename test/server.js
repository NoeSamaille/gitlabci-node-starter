const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

chai.use(chaiHttp);
describe('server', function () {
    describe('GET /', function () {
        it('should get Hello World', function (done) {
            chai.request(server)
                .get('/')
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eql('Hello World!');
                    done();
                });
        });
    });
});